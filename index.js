const fs = require('fs');
const csv = require('csvtojson');
const path = require('path');

const MATCHES_FILE_PATH = path.join(__dirname, "./src/data/matches.csv");
const DELIVERIES_FILE_PATH = path.join(__dirname, "./src/data/deliveries.csv");
const OUTPUT_DIRECTORY = path.join(__dirname, './src/public/output/');

const matchesPlayedPerYear = require('./src/server/1-matches-per-year');
const matchesWonPerTeamPerYear = require('./src/server/2-matches-won-per-team-per-year');
const extraRunConcededByEachTeam = require('./src/server/3-extra-runs-conceded-per-team');
const topTenEconomicalBowlersByYear = require('./src/server/4-top-10-economical-bowlers-by-year');
const tossWinMatchWin = require('./src/server/5-won-toss-and-match-by-each-team');
const highestPlayerOfMatchesWinner = require('./src/server/6-highest-player-of-the-match-each-season');
const strikeRateOfBatsmanPerYear = require('./src/server/7-strike-rate-of-batsman-per-year');
const highestPlayerDismissed = require('./src/server/8-highest-player-dismissed');
const bestEconomicBowlerInSuperOver = require('./src/server/9-best-economy-bowler-in-super-over');

function main() {
  try{
     csv()
    .fromFile(MATCHES_FILE_PATH)
    .then(matches => {
      csv()
        .fromFile(DELIVERIES_FILE_PATH)
        .then(deliveries => {

          function createFile(filePath,data){
            fs.writeFile(filePath,data,(error)=>{
              if(error){
                console.log(error);
              }
            })
          }
          
          try {
            let totalMatchesPlayedPerYear = matchesPlayedPerYear(matches);
            totalMatchesPlayedPerYear={matchesPlayedPerYear:totalMatchesPlayedPerYear}
            totalMatchesPlayedPerYear = JSON.stringify(totalMatchesPlayedPerYear, null, 4);
            createFile(path.join(OUTPUT_DIRECTORY, 'matches-per-year.json'), totalMatchesPlayedPerYear)
          }
          catch (error) {
            console.error(error);
          }

          try {
            let totalMatchesWonPerTeamPerYear = matchesWonPerTeamPerYear(matches);
            totalMatchesWonPerTeamPerYear={Matcheswon:totalMatchesWonPerTeamPerYear}
            totalMatchesWonPerTeamPerYear = JSON.stringify(totalMatchesWonPerTeamPerYear, null, 4);
            createFile(path.join(OUTPUT_DIRECTORY, 'matches-won-per-team-per-year.json'), totalMatchesWonPerTeamPerYear)
          }
          catch (error) {
            console.error(error);
          }

          try {
            const extraRunsYear = '2016';
            let extraRunsConcededPerTeam = extraRunConcededByEachTeam(matches, deliveries, extraRunsYear);
            extraRunsConcededPerTeam={ExtraRunConceded:extraRunsConcededPerTeam}
            extraRunsConcededPerTeam = JSON.stringify(extraRunsConcededPerTeam, null, 4);
            createFile(path.join(OUTPUT_DIRECTORY, `extra-runs-conceded-per-team-for-2016.json`), extraRunsConcededPerTeam)
          }
          catch (error) {
            console.error(error);
          }

          try {
            const topTenEconomicBowlerYear = '2015';
            let topTenEconomicBowlers = topTenEconomicalBowlersByYear(matches, deliveries, topTenEconomicBowlerYear);
            topTenEconomicBowlers={TopEconomicalBowlers:topTenEconomicBowlers};
            topTenEconomicBowlers = JSON.stringify(topTenEconomicBowlers, null, 4);
            createFile(path.join(OUTPUT_DIRECTORY, `top-10-economical-bowlers-by-year-2015.json`), topTenEconomicBowlers)
          }
          catch (error) {
            console.error(error);
          }

          try {
            let wonTossAndMatchByEachTeam = tossWinMatchWin(matches);
            wonTossAndMatchByEachTeam={WonTossAndMatch:wonTossAndMatchByEachTeam}
            wonTossAndMatchByEachTeam = JSON.stringify(wonTossAndMatchByEachTeam, null, 4);
            createFile(path.join(OUTPUT_DIRECTORY, 'won-toss-and-match-by-each-team.json'), wonTossAndMatchByEachTeam)
          }
          catch (error) {
            console.error(error);
          }

          try {
            let playerOfTheMatchEachSeason = highestPlayerOfMatchesWinner(matches);
            playerOfTheMatchEachSeason={playerOfTheMatchAwardsEachSeason:playerOfTheMatchEachSeason}
            playerOfTheMatchEachSeason = JSON.stringify(playerOfTheMatchEachSeason, null, 4);
            createFile(path.join(OUTPUT_DIRECTORY, 'highest-player-of-the-match-each-season.json'), playerOfTheMatchEachSeason)
          }
          catch (error) {
            console.error(error);
          }

          try {
            let strikeRateOfEveryBatsmanPerYear = strikeRateOfBatsmanPerYear(matches, deliveries);
            strikeRateOfEveryBatsmanPerYear={strikeRate:strikeRateOfEveryBatsmanPerYear};
            strikeRateOfEveryBatsmanPerYear = JSON.stringify(strikeRateOfEveryBatsmanPerYear, null, 4);
            createFile(path.join(OUTPUT_DIRECTORY, 'strike-rate-of-batsman-per-year.json'), strikeRateOfEveryBatsmanPerYear)
          }
          catch (error) {
            console.error(error);
          }

          try {
            let highestPlayerDismissedByAnotherPlayer = highestPlayerDismissed(deliveries);
            highestPlayerDismissedByAnotherPlayer = JSON.stringify(highestPlayerDismissedByAnotherPlayer, null, 4);
            createFile(path.join(OUTPUT_DIRECTORY, 'highest-player-dismissed.json'), highestPlayerDismissedByAnotherPlayer)
          }
          catch (error) {
            console.error(error);
          }

          try {
            let topEconomicBowlerInSuperOver = bestEconomicBowlerInSuperOver(deliveries);
            topEconomicBowlerInSuperOver = JSON.stringify(topEconomicBowlerInSuperOver, null, 4);
            createFile(path.join(OUTPUT_DIRECTORY, 'best-economy-bowler-in-super-over.json'), topEconomicBowlerInSuperOver);
          }
          catch (error) {
            console.error(error);
          }

        })
    })
  }catch(error) {
    console.log(error)
  }
 
}

main();
