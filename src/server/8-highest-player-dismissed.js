const highestPlayerDismissed = (deliveries) => {

    if (deliveries === undefined || !Array.isArray(deliveries)) {
        return {};
    }

    const dismissalObj={}
            
    deliveries.map((each)=>{
        if(each.player_dismissed!==''){
            if((each.player_dismissed in dismissalObj)){
                if (each.bowler in dismissalObj[each.player_dismissed]){
                    dismissalObj[each.player_dismissed][each.bowler]+=1;
                }else{
                    dismissalObj[each.player_dismissed][each.bowler]=1
                }
                
            }else{
                dismissalObj[each.player_dismissed]={}
                dismissalObj[each.player_dismissed][each.bowler]=1
                
            }
            
        }



    })

    const dismissedCounts = Object.keys(dismissalObj).map((dismissedBatsman) =>{
        const sortingObj=Object.entries(dismissalObj[dismissedBatsman]).sort((a,b)=>b[1]-a[1]).slice(0,1).flat(1) 
        return [dismissedBatsman,sortingObj]    
    });

    const mostDismissed=dismissedCounts.sort((a,b)=>b[1][1]-a[1][1]).slice(0,1).flat(1)
    const highestDismissedByAnotherPlayer={}
    const dismissedObj={}
    dismissedObj['dismissedBy']=mostDismissed[1][0]
    dismissedObj['dismissedCount']=mostDismissed[1][1]
    highestDismissedByAnotherPlayer[mostDismissed[0]]=dismissedObj
    
    return highestDismissedByAnotherPlayer
}

module.exports = highestPlayerDismissed;