const bestEconomicBowlerInSuperOver = (deliveries) => {

    if (deliveries === undefined || !Array.isArray(deliveries)) {
        return {};
    }

    const bowlersData={};
    deliveries.map((element)=>{
        if(element.is_super_over!=='0'){
            if(!(element.bowler in bowlersData)){
                bowlersData[element.bowler] = {'Balls':1, 'Runs':Number(element.total_runs)};
            }else{
                const obj = bowlersData[element.bowler];
                obj.Balls +=1;
                obj.Runs +=  Number(element.total_runs);
            }
        }

    })


    const bowlersDataArray = Object.keys(bowlersData).map((bowler)=>{
        return {bowler,'economy':Number((bowlersData[bowler].Runs/(bowlersData[bowler].Balls/6)).toFixed(2))};
    })

    const bestEconomyInSuperOvers=(bowlersDataArray.sort((a,b)=> a.economy-b.economy)).slice(0,1);
    return bestEconomyInSuperOvers
        


}

module.exports = bestEconomicBowlerInSuperOver;