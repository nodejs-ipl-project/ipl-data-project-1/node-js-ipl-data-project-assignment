const topTenEconomicalBowlersByYear = (matches, deliveries, year) => {

    if (matches === undefined || deliveries === undefined || year == undefined || !Array.isArray(matches) || !Array.isArray(deliveries)) {
        return {};
    }

    let matchesByYear=[]
        
        
        for (matchobj of matches){
            if(matchobj.season==='2015'){
                let id = matchobj.id;
                for (deliveryobj of deliveries){
                    if(deliveryobj.match_id===id){
                        matchesByYear.push(deliveryobj);
                    }
                }

            }
        }
        const bowlersData={};

        matchesByYear.map((element)=>{
                    if(!(element.bowler in bowlersData)){
                        bowlersData[element.bowler] = {'Balls':1, 'Runs':Number(element.total_runs)};
                    }else{
                        const obj = bowlersData[element.bowler];
                        obj.Balls +=1;
                        obj.Runs +=  Number(element.total_runs);
                    }

        })


        const bowlersDataArray = Object.keys(bowlersData).map((bowler)=>{
            return {bowler,'economy':Number((bowlersData[bowler].Runs/(bowlersData[bowler].Balls/6)).toFixed(2)),'balls':bowlersData[bowler].Balls,'runs':bowlersData[bowler].Runs};
        })

        const sortedArray=bowlersDataArray.sort((a,b)=> a.economy-b.economy);
        const slicedArray=sortedArray.slice(0,10);
        topEconomicalObject={}
        slicedArray.map((bowlerObj,index)=>{
            topEconomicalObject[bowlerObj.bowler]=bowlerObj.economy
        })
        return topEconomicalObject;
    
}

module.exports = topTenEconomicalBowlersByYear;