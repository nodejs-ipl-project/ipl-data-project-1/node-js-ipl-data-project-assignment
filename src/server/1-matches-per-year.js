const matchesPlayedPerYear = (matches) => {

    if (matches === undefined || !Array.isArray(matches)) {
        return {};
    }

    let matchesPlayedInEachYear={};
        
            for (let match of matches){
                let season=match.season;
                if(season in matchesPlayedInEachYear){
                    matchesPlayedInEachYear[season]+=1;
                }
                else{
                    matchesPlayedInEachYear[season]=1;
                }
            }

    return matchesPlayedInEachYear;
}

module.exports = matchesPlayedPerYear;
