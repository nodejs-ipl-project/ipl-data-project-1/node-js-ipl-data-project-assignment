const tossWinMatchWin=(matches)=> {

    if (matches === undefined || !Array.isArray(matches)) {
        return {};
    }

    let teamWinDataObject = {};
    matches.map((element)=>{
        if(element.toss_winner==element.winner){
            if(!(element.winner in teamWinDataObject)){
                teamWinDataObject[element.winner] = 1;
            }else{
                teamWinDataObject[element.winner]+=1;
            }
        }
    })
    return teamWinDataObject;
}

module.exports = tossWinMatchWin;

