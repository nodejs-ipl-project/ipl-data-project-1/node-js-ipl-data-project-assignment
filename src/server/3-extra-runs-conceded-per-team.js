const extraRunConcededByEachTeamByYear = (matches, deliveries, year) => {

       if (matches === undefined || deliveries === undefined || year == undefined || !Array.isArray(matches) || !Array.isArray(deliveries)) {
              return {};
       }

       let matchesByYear=[]
            for (matchObj of matches){
                if(matchObj.season===year){
                    let id = matchObj.id;
                    for (deliveryObj of deliveries){
                        if(deliveryObj.match_id===id){
                            matchesByYear.push(deliveryObj);
                        }
                    }

                }
            }

            let extraRunsPerTeam = {};

            for(let obj of matchesByYear){
                let team=obj.bowling_team;
                if(team in extraRunsPerTeam){
                    extraRunsPerTeam[team]+=parseInt(obj.extra_runs)
                }
                else{
                    extraRunsPerTeam[team]=0;
                }
            }

       return extraRunsPerTeam;
}

module.exports = extraRunConcededByEachTeamByYear;
