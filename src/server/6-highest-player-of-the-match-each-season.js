const highestPlayerOfMatchesWinner = (matches) => {

    if (matches === undefined || !Array.isArray(matches)) {
        return {};
    }

    const playerOfTheMatch = {}
        const seasonsArray = new Set(matches.map((match)=>match.season))

        for (let year of seasonsArray) {
        const awardsInSeason = {}
            for (let match of matches) {
                if (match.season === year) {
                    if (match.player_of_match in awardsInSeason) {
                        awardsInSeason[match.player_of_match]+= 1
                    } else {
                        awardsInSeason[match.player_of_match] =1
                    }
                }
            }
            const sortedArray=((Object.entries(awardsInSeason)).sort((a,b)=>b[1]-a[1])).slice(0,1)
            const playerArray=sortedArray.map((eachPair)=>{
                const obj={}
                obj[eachPair[0]]=eachPair[1]
                return obj;
            })
            playerOfTheMatch[year]=playerArray[0]
        }
        return playerOfTheMatch
}

module.exports = highestPlayerOfMatchesWinner;
