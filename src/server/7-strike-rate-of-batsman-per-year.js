const strikeRateOfBatsmanPerYear = (matches, deliveries) => {

    if (matches === undefined || deliveries === undefined || !Array.isArray(matches) || !Array.isArray(deliveries)) {
        return {};
    }

    const seasonsArray = new Set(matches.map((match)=>match.season))
    const strikeRateObj={}

    for(let year of seasonsArray){
    
        let matchesByYear=[]
            
        for (matchObj of matches){
            if(matchObj.season===year){
                let id = matchObj.id;
                for (deliveryObj of deliveries){
                    if(deliveryObj.match_id===id){
                        matchesByYear.push(deliveryObj);
                    }
                }

            }
        }
        const batsmanData={};

        matchesByYear.map((element)=>{
                    if(!(element.batsman in batsmanData)){
                        batsmanData[element.batsman] = {'Balls':1, 'Runs':Number(element.batsman_runs)};
                    }else{
                        const obj = batsmanData[element.batsman];
                        obj.Balls +=1;
                        obj.Runs +=  Number(element.batsman_runs);
                    }

        })

        const bowlersData={}
        Object.keys(batsmanData).map((batsman)=>{
            const strike_rate=Number((batsmanData[batsman].Runs/(batsmanData[batsman].Balls)*100).toFixed(2))
            bowlersData[batsman]=strike_rate;
        })

        strikeRateObj[year]=bowlersData;
        
    }
            
    return strikeRateObj;
}

module.exports = strikeRateOfBatsmanPerYear;
