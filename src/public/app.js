
function fetchAndVisualizeData1() {
    fetch("./output/matches-per-year.json")
      .then(r => r.json())
      .then(visualizeData1);
  }
  fetchAndVisualizeData1();
  
  function fetchAndVisualizeData2() {
    fetch("./output/matches-won-per-team-per-year.json")
      .then(r => r.json())
      .then(visualizeData2)
  }
  fetchAndVisualizeData2();
  
  function fetchAndVisualizeData3() {
    fetch("./output/extra-runs-conceded-per-team-for-2016.json")
      .then(r => r.json())
      .then(visualizeData3)
    }
  fetchAndVisualizeData3();
  
  function fetchAndVisualizeData4() {
      fetch("./output/top-10-economical-bowlers-by-year-2015.json")
        .then(r => r.json())
        .then(visualizeData4)
   }
  fetchAndVisualizeData4();
  
  function fetchAndVisualizeData5() {
    fetch("./output/strike-rate-of-batsman-per-year.json")
      .then(r => r.json())
      .then(visualizeData5)
  }
  fetchAndVisualizeData5();

  function fetchAndVisualizeData6() {
    fetch("./output/won-toss-and-match-by-each-team.json")
      .then(r => r.json())
      .then(visualizeData6)
  }
  fetchAndVisualizeData6();

  function fetchAndVisualizeData7() {
    fetch("./output/highest-player-of-the-match-each-season.json")
      .then(r => r.json())
      .then(visualizeData7)
  }
  fetchAndVisualizeData7();
  
  
  function visualizeData1(data) {
    visualizeMatchesPlayedPerYear(data.matchesPlayedPerYear);
    return;
  }
  
  function visualizeData2(data) {
    visualizeMatchesWonByEachTeam(data.Matcheswon);
    return;
  }
  
  function visualizeData3(data) {
    visualizeExtraRuns(data.ExtraRunConceded);
    return;
  }
  
  function visualizeData4(data) {
    visualizeEconomicalBowlers(data.TopEconomicalBowlers);
    return;
  }
  
  function visualizeData5(data) {
    visualizeStrikeRate(data.strikeRate);
    return;
  }

  function visualizeData6(data){
    visualizeTossAndMatch(data.WonTossAndMatch)
  }

  function visualizeData7(data){
    visualizePlayerOftheMatchAwards(data.playerOfTheMatchAwardsEachSeason)
  }
  
    //1st problem - Total Number of Matches Played each Year
  
  function visualizeMatchesPlayedPerYear(matchesPlayedPerYear) {
    const seriesData = [];
    for (let year in matchesPlayedPerYear) {
      seriesData.push([year, matchesPlayedPerYear[year]]);
    }
  
    Highcharts.chart("matches-played-per-year", {
      chart: {
        type: "column"
      },
      title: {
        text: "1. Matches Played Per Year"
      },
      subtitle: {
        text:
          'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
      },
      xAxis: {
        type: "category"
      },
      yAxis: {
        min: 0,
        title: {
          text: "Matches"
        }
      },
      series: [
        {
          name: "Years",
          data: seriesData,
          dataLabels:{
            enabled:true
          }
        }
      ]
    });
  }
  
  //2nd Problem - No.of Matches Won by Each Team over all the years
  
  function visualizeMatchesWonByEachTeam(MatchesWon) {
    let years = [];
    let allTeam = [];
    for (let key in MatchesWon) {
        years.push(key)
        for (nameOfTeam in MatchesWon[key]) {
            if (!allTeam.includes(nameOfTeam)) {
                allTeam.push(nameOfTeam);
            }
        }
    }
    let seriesData = [];
    for (let j in allTeam) {
        let arr = [];
        for (let i in years) {
            if (MatchesWon[years[i]].hasOwnProperty(allTeam[j])) {
                arr.push(MatchesWon[years[i]][allTeam[j]]);
            }
            else {
                arr.push(null);
            }
        }
        let obj = {
            'name': allTeam[j],
            'data': arr,
            dataLabels:{
              enabled:true
            }
        }
        seriesData.push(obj);
    }
    
      Highcharts.chart('matches-won-by-each-team', {
        chart: {
            type: 'column'
        },
        title: {
            text: '2: Matches Won By Each Team in all IPL'
        },
        subtitle: {
            text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
        },
        xAxis: {
            categories: 
              years,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Matches Won'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: seriesData
      });
    }
  
  //3rd Problem- Extra Runs Conceded by Each Team in 2016(Third Problem)
  
  function visualizeExtraRuns(ExtraRunConceded) {
    const seriesData = [];
    for (let team in ExtraRunConceded) {
      seriesData.push([team, ExtraRunConceded[team]]);
    }
  
    Highcharts.chart("extra-runs-conceded", {
      chart: {
        type: "column"
      },
      title: {
        text: "3. Extra Runs Conceded"
      },
      subtitle: {
        text:
          'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
      },
      xAxis: {
        type: "category"
      },
      yAxis: {
        min: 0,
        title: {
          text: "Extra Runs"
        }
      },
      series: [
        {
          name:"Teams",
          data: seriesData,
          dataLabels:{
            enabled:true
          }
        }
      ]
    });
  }
  
  //Top 10 economical Bowlers(Fourth Problem)
  
  function visualizeEconomicalBowlers(TopEconomicalBowlers) {
    const seriesData = [];
    for (let economical in TopEconomicalBowlers) {
      seriesData.push([economical,TopEconomicalBowlers[economical]]);
    }
  
    Highcharts.chart("economical-bowlers", {
      chart: {
        type: "column"
      },
      title: {
        text: "4. Top 10 Economical Bowlers"
      },
      subtitle: {
        text:
          'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
      },
      xAxis: {
        type: "category"
      },
      yAxis: {
        min: 0,
        title: {
          text: "Economy"
        }
      },
      series: [
        {
          name:"Players",
          data: seriesData,
          dataLabels:{
            enabled:true
          }
        }
      ]
    });
  }
  
  //5.Strike Rate of each Batsman in each season

  function visualizeStrikeRate(StrikeRate) {
    let years = [];
    let allPlayers = [];
    for (let key in StrikeRate) {
        years.push(key)
        for (nameOfPlayer in StrikeRate[key]) {
            if (!allPlayers.includes(nameOfPlayer)) {
                allPlayers.push(nameOfPlayer);
            }
        }
    }
    let seriesData = [];
    for (let j in allPlayers) {
        let arr = [];
        for (let i in years) {
            if (StrikeRate[years[i]].hasOwnProperty(allPlayers[j])) {
                arr.push(StrikeRate[years[i]][allPlayers[j]]);
            }
            else {
                arr.push(null);
            }
        }
        let obj = {
            'name': allPlayers[j],
            'data': arr,
            dataLabels:{
              enabled:true
            }
        }
        seriesData.push(obj);
    }
    
    Highcharts.chart('StrikeRate', {
      chart: {
          type: 'column'
      },
      title: {
          text: '5: StrikeRate of each player in each season'
      },
      subtitle: {
          text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
      },
      xAxis: {
          categories: 
            years,
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
              text: 'StrikeRate'
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      series: seriesData
    });
  }
  
// 6.won toss and match by each team

  function visualizeTossAndMatch(wonTossAndMatches) {
    const seriesData = [];
    for (let team in wonTossAndMatches) {
      seriesData.push([team, wonTossAndMatches[team]]);
    }
  
    Highcharts.chart("won-toss-and-match", {
        chart: {
          type: "column"
        },
        title: {
          text: "6. Won Toss And Match by each team"
        },
        subtitle: {
          text:
            'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
        },
        xAxis: {
          type: "category"
        },
        yAxis: {
          min: 0,
          title: {
            text: "Won Toss And Match Count"
          }
        },
        series: [
          {
            name:"Teams",
            data: seriesData,
            dataLabels:{
              enabled:true
            }
          }
        ]
      });
  }
    
    // 7. Highest player of matches awards in each season

    function visualizePlayerOftheMatchAwards(PlayerOftheMatchAwards) {
      let years = [];
      let allPlayers = [];
      for (let key in PlayerOftheMatchAwards) {
          years.push(key)
          for (let nameOfPlayer in PlayerOftheMatchAwards[key]) {
              if (!allPlayers.includes(nameOfPlayer)) {
                  allPlayers.push(nameOfPlayer);
              }
          }
      }
      let seriesData = [];
      for (let j in allPlayers) {
          let arr = [];
          for (let i in years) {
              if (PlayerOftheMatchAwards[years[i]].hasOwnProperty(allPlayers[j])) {
                  arr.push(PlayerOftheMatchAwards[years[i]][allPlayers[j]]);
              }
              else {
                  arr.push(null);
              }
              
          }
          console.log(arr)
          let obj = {
              'name': allPlayers[j],
              'data': arr,
              dataLabels:{
                enabled:true
              }
          }
          seriesData.push(obj);
      }
      
        Highcharts.chart('playerOftheMatchAwards', {
          chart: {
              type: 'column'
          },
          title: {
              text: '7: Highest Player Of the Match Awards in each season'
          },
          subtitle: {
              text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
          },
          xAxis: {
              categories: 
                years,
              crosshair: true
          },
          yAxis: {
              min: 0,
              title: {
                  text: 'PlayerOfTheMatchAwards'
              }
          },
          tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0; width:100px">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0
              }
          },
          series: seriesData

        });
      }
